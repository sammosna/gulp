/*** * * * * * * * * * * * * * * * * * ***/
/*     _____       _        __ _ _          */
/*    / ____|     | |      / _(_) |         */
/*   | |  __ _   _| |_ __ | |_ _| | ___     */
/*   | | |_ | | | | | '_ \|  _| | |/ _ \    */
/*   | |__| | |_| | | |_) | | | | |  __/    */
/*    \_____|\__,_|_| .__/|_| |_|_|\___|    */
/*                  | |                     */
/*                  |_|   by sammosna.it    */
/*** * * * * * * * * * * * * * * * * * ***/
/*VERSION: 0.9.1 */

//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: I N S T A L L : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//

/*
  npm link fs request gulp gulp-if gulp-sass gulp-flatten gulp-plumber gulp-clean-css gulp-autoprefixer gulp-javascript-obfuscator gulp-sourcemaps browserify vinyl-transform vinyl-source-stream vinyl-buffer chalk vinyl-sourcemaps-apply babelify babel-plugin-console-log @babel/plugin-transform-classes @babel/core @babel/preset-env gulp-imagemin @babel/parser @babel/generator
*/

//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: R E Q U I R E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//

const fs = require('fs'),
  request = require('request'),
  gulp = require('gulp'),
  gulpif = require('gulp-if'),
  sass = require('gulp-sass'),
  babelify = require('babelify'),
  flatten = require('gulp-flatten'),
  plumber = require('gulp-plumber'),
  cleanCSS = require('gulp-clean-css'),
  autoprefixer = require('gulp-autoprefixer'),
  javascriptObfuscator = require('gulp-javascript-obfuscator'),
  sourcemaps = require("gulp-sourcemaps"),
  browserify = require('browserify'),
  transform = require('vinyl-transform'),
  source = require('vinyl-source-stream'),
  buffer = require('vinyl-buffer'),
  imagemin = require('gulp-imagemin'),
  chalk = require('chalk')


//
// ──────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: G L O B A L   V A R I A B L E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────
//

// INITIALIZE SETTINGS OBJECT
var settings = null

// DEFINING CHALK THEME
const newPhase = chalk.bgBlue


//
// ──────────────────────────────────────────────── I ──────────
//   :::::: I N I T : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────
//





gulp.task('init', async function () {
  console.log(chalk.bgWhite.black(" • WELCOME • "))

  // CHECK BROWSERS LIST
  if (!fs.existsSync(".browserslistrc")) {
    fs.writeFileSync(".browserslistrc", "last 2 versions\n\ > 1%\n\ ie 8\n\ ie 9 \n\ not dead")
  }

  // CHECK FOR UPDATES
  await UpdatesController()

  // CHECK SETTINGS
  await CheckSettings()

  settings = require('./gulpfile.settings')

});


gulp.task('compile', gulp.parallel(watch,
  done => {
    if (settings.modules.scss) styles()
    if (settings.modules.babel) scripts()
    if (settings.modules.imagemin) images()
    done();
  }
));

gulp.task('default', gulp.series('init', 'compile'));


//
// ──────────────────────────────────────────────────── I ──────────
//   :::::: S T Y L E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────
//

function styles() {
  return gulp
    .src(settings.paths.styles.src, {
      sourcemaps: true
    })
    .pipe(sass())
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(flatten())
    .pipe(gulp.dest(settings.paths.styles.dest));
}


//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: S C R I P T S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//

function scripts() {

  return browserify({
    entries: settings.paths.script.src
  })
    .transform(babelify.configure({
      // presets: ["es2015"]
      // presets: ['@babel/env']
      presets: ['@babel/preset-env'],
      plugins: [
        "@babel/plugin-transform-classes",
        ["console-log", {
          "loggers": [{
            "pattern": "console"
          },
          {
            "pattern": "log",
            "method": "trace"
          }
          ]
        }]
      ]
    }))
    .bundle()
    .pipe(source("main.js"))
    .pipe(buffer())
    .pipe(plumber())
    .pipe(gulpif((settings.modules.jsObfuscator), javascriptObfuscator({
      compact: true
    })))
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(settings.paths.script.dest));

}


//
// ──────────────────────────────────────────────────── I ──────────
//   :::::: I M A G E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────
//

function images() {
  return gulp.src(settings.paths.images.src)
    .pipe(imagemin())
    .pipe(flatten())
    .pipe(gulp.dest(settings.paths.images.dest))
}


//
// ────────────────────────────────────────────────── I ──────────
//   :::::: W A T C H : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────
//

function watch() {

  if (settings.modules.scss) gulp.watch(settings.paths.styles.src, styles);
  if (settings.modules.babel) gulp.watch(settings.paths.script.src, scripts);
  if (settings.modules.imagemin) gulp.watch(settings.paths.images.src, images);

}


//
// ──────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: U P D A T E S   C O N T R O L L E R : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────────────
//

async function UpdatesController() {
  return new Promise(async function (resolve, reject) {
    process.stdout.write(newPhase(" Updates "));
    let res = await doRequest('https://gitlab.com/sammosna/gulp/raw/master/gulpfile.js');
    var gfOnline = res
    var vOnline = gfOnline.match("/\*VERSION:(.*)\*/")[1].replace("*", "").trim()

    fs.readFile('./gulpfile.js', 'utf8', function (err, contents) {
      var vLocal = contents.match("/\*VERSION:(.*)\*/")[1].replace("*", "").trim()

      if (vOnline != vLocal) {
        process.stdout.write(chalk.bgRed(" Update your gulpfile! [" + vLocal + " ❯ " + vOnline + "] "))
        process.stdout.write(" → http://bit.ly/smGulp")
        resolve()
      } else {
        // process.stdout.write(chalk.bgGreen(" Your gulpfile is up-to-date "))
        process.stdout.write(chalk.bgGreen(" ✔ "))
        resolve()
      }
      process.stdout.write("\n")
    });
  })
}

//
// ────────────────────────────────────────────────────────────────── I ──────────
//   :::::: C H E C K   S E T T I G S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────
//
async function CheckSettings() {
  return new Promise(async function (resolve, reject) {
    process.stdout.write(newPhase(" Settings "));
    if (!fs.existsSync("gulpfile.settings.js")) {

      let res = await doRequest('https://gitlab.com/sammosna/gulp/raw/master/gulpfile.settings.js');
      fs.writeFileSync("gulpfile.settings.js", res)
      process.stdout.write(chalk.bgYellow.black(" Please edit your gulpfile.settigs.js "))

      process.stdout.write("\n")
      resolve()

    } else {
      process.stdout.write(chalk.bgGreen.white(" ✔ "))
      process.stdout.write("\n")
      resolve()
    }
  })
}



// ────────────────────────────────────────────────────────────────────────────────
function doRequest(url) {
  return new Promise(function (resolve, reject) {
    request(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
}
