exports.modules = {
  scss: true,
  imagemin: true,
  babel: true,
  jsObfuscator: false
}

exports.preferencies = {

}

exports.paths = {
  styles: {
    src: 'src/{css,scss}/*.scss',
    dest: 'dist/css'
  },
  script: {
    src: ['src/js/main.js'],
    dest: 'dist/js/'
  },
  images: {
    src: 'src/img/*',
    dest: 'dist/img/'
  }
};