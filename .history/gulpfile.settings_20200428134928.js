exports.modules = {
  scss: true,
  imagemin: true,
  babel: true,
  jsObfuscator: false
}

exports.preferencies = {
  packageManager: 'yarn' // yarn | npm
}

exports.paths = {
  styles: {
    src: 'src/{css,scss}/*.scss',
    dest: 'static/css'
  },
  script: {
    src: './src/js/**/*.js', // glob pattern
    dest: 'static/js/'
  },
  images: {
    src: 'src/img/*',
    dest: 'static/img/'
  }
};

exports.dependencies = [
  'fs',
  'request',
  'gulp',
  'gulp-if',
  'gulp-sass',
  'gulp-flatten',
  'gulp-plumber',
  'gulp-clean-css',
  'gulp-autoprefixer',
  'gulp-javascript-obfuscator',
  'gulp-sourcemaps',
  'browserify',
  'vinyl-transform',
  'vinyl-source-stream',
  'vinyl-buffer',
  'chalk',
  'vinyl-sourcemaps-apply',
  'babelify',
  'babel-plugin-console-log',
  '@babel/plugin-transform-classes',
  '@babel/core',
  '@babel/preset-env',
  'gulp-imagemin',
  '@babel/parser',
  '@babel/generator',
  'auto-changelog',
  'node-sass'
]